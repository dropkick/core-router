<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class RouteTest extends TestCase {

  public function testWithPath() {
    $route = new Route(['path' => 'test']);
    $modified_route = $route->withPath('path');
    $this->assertEquals('path', $modified_route->getPath());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithOptions() {
    $route = new Route(['path' => 'test', 'options' => ['head' => TRUE]]);
    $modified_route = $route->withOptions(['tail' => TRUE]);
    $this->assertEquals(['tail' => TRUE], $modified_route->getOptions());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithMethods() {
    $route = new Route(['path' => 'test', 'methods' => ['head' => TRUE]]);
    $modified_route = $route->withMethods(['tail' => TRUE]);
    $this->assertEquals(['tail' => TRUE], $modified_route->getMethods());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithDefaults() {
    $route = new Route(['path' => 'test', 'defaults' => ['head' => TRUE]]);
    $modified_route = $route->withDefaults(['tail' => TRUE]);
    $this->assertEquals(['tail' => TRUE], $modified_route->getDefaults());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithRequirements() {
    $route = new Route(['path' => 'test', 'requirements' => ['head' => TRUE]]);
    $modified_route = $route->withRequirements(['tail' => TRUE]);
    $this->assertEquals(['tail' => TRUE], $modified_route->getRequirements());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testOptions() {
    $route = new Route(['path' => 'test', 'options' => ['head' => TRUE]]);

    $this->assertTrue($route->hasOption('head'));
    $this->assertFalse($route->hasOption('tail'));
    $this->assertEquals(TRUE, $route->getOption('head', FALSE));
    $this->assertEquals('tail', $route->getOption('tail', 'tail'));
  }

}
