<?php

namespace Dropkick\Core\Router\Exception;

use PHPUnit\Framework\TestCase;

class MethodNotAllowedExceptionTest extends TestCase {
  
  public function testGetMethods() {
    $methods = ['GET'];
    $exception = new MethodNotAllowedException($methods);
    
    $this->assertEquals($methods, $exception->getMethods());
  }

}
