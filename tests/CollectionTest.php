<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase {

  public function testConstruct() {
    $routes = [
      'all' => new Route(['path' => 'all'])
    ];

    $collection = new Collection();
    $this->assertEquals(0, count($collection->all()));

    $collection = new Collection($routes);
    $this->assertEquals(1, count($collection->all()));
    $this->assertEquals('all', $collection->get('all')->getPath());

    foreach ($collection as $key => $route) {
      $this->assertTrue(isset($routes[$key]));
      $this->assertEquals($routes[$key]->getPath(), $route->getPath());
    }

    $collection->remove('undefined');
    $this->assertEquals(1, count($collection));

    $collection->remove('all');
    $this->assertEquals(0, count($collection));
  }

  public function testReplacement() {
    $routes = [
      'all' => new Route(['path' => 'all'])
    ];

    $replacement = [
      'items' => new Route(['path' => 'items']),
      'home' => new Route(['path' => 'home'])
    ];

    $collection = new Collection($routes);

    $collection->setCollection(new Collection($replacement));

    $this->assertEquals(2, count($collection));
    $this->assertNull($collection->get('all'));
  }

  public function testAddition() {
    $routes = [
      'home' => new Route(['path' => 'home']),
      'all' => new Route(['path' => 'all'])
    ];

    $replacement = [
      'items' => new Route(['path' => 'items']),
      'home' => new Route(['path' => 'home'])
    ];

    $collection = new Collection($routes);

    $collection->addCollection(new Collection($replacement));

    $this->assertEquals(3, count($collection));

    $all = $collection->all();
    $this->assertEquals(2, array_search('home', array_keys($all)));
  }

}
