<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase {

  public function testCreateRoute() {
    $factory = new Factory();
    $collection = $factory->createRoute(['path' => 'path']);
    $this->assertEquals(Route::class , get_class($collection));
  }

  public function testCreateCollection() {
    $factory = new Factory();
    $collection = $factory->createCollection();
    $this->assertEquals(Collection::class , get_class($collection));
  }

}
