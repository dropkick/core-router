<?php

namespace Dropkick\Core\Router;

use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class ContextFactoryTest extends TestCase {

  public function testContext() {
    $request = new Request('post', 'https://example.com:8443/base/path');

    $factory = new ContextFactory();

    $context = $factory->getRequestContext($request);

    $this->assertEquals('8443', $context->getPort());
    $this->assertEquals('base/path', $context->getPathInfo());
    $this->assertEquals('POST', $context->getMethod());
  }

  public function testBaseContext() {
    $request = new Request('patch', 'https://example.com:8443/base/path');

    $factory = new ContextFactory();
    $factory->setBaseUrl('https://example.com:8443/base');

    $context = $factory->getRequestContext($request);

    $this->assertEquals('path', $context->getPathInfo());
    $this->assertEquals('PATCH', $context->getMethod());
  }

}
