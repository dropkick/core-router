<?php

namespace Dropkick\Core\Router;

use Dropkick\Core\Router\Exception\MethodNotAllowedException;
use Dropkick\Core\Router\Exception\ResourceNotFoundException;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

class RouterTest extends TestCase {

  protected $routes;
  protected $provider;
  protected $compiler;
  protected $factory;
  protected $builder;
  protected $matchFactory;

  /**
   * @var \Dropkick\Core\Router\ContextFactory
   */
  protected $contextFactory;

  public function setUp(): void {
    $this->routes = [
      'home' => new Route(['path' => '/home']),
      'home.action' => new Route(['path' => '/home/{action}', 'defaults' => ['action' => 'view'], 'methods' => ['GET']]),
      'user.view' => new Route(['path' => '/user/{user}']),
      'user.login' => new Route(['path' => '/user/login']),
      'methods' => new Route(['path' => '/methods', 'methods' => ['GET']])
    ];

    $this->compiler = new Compiler();
    $this->factory = new Factory();
    $this->builder = new Builder();
    $this->matchFactory = new MatchFactory();
    $this->contextFactory = new ContextFactory();

    $this->provider = $this->getMockBuilder(ProviderInterface::class)
      ->getMock();
    $this->provider->method('getAllRoutes')->willReturn($this->routes);
  }

  protected function getRouter() {
    return new Router(
      $this->provider,
      $this->compiler,
      $this->factory,
      $this->builder,
      $this->matchFactory,
      $this->contextFactory
    );
  }

  public function testNoMatch() {
    $request = new Request('GET', 'http://example.com/no/match');
    $context = $this->contextFactory->getRequestContext($request);

    $this->expectException(ResourceNotFoundException::class);
    $this->getRouter()->match($context);
  }

  public function testNoMethod() {
    $request = new Request('POST', 'http://example.com/methods');
    $context = $this->contextFactory->getRequestContext($request);

    $this->expectException(MethodNotAllowedException::class);
    $this->getRouter()->match($context);
  }

  public function testMethodHead() {
    $request = new Request('HEAD', 'http://example.com/home/value');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()->match($context);
    $this->assertEquals('home.action', $match->getRouteName());
    $this->assertEquals('value', $match->getParameter('action'));
  }

  public function testSimilarUrl() {
    $request = new Request('GET', 'http://example.com/user/login');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()->match($context);
    $this->assertEquals('user.login', $match->getRouteName());
  }

  public function testFilter() {
    $request = new Request('GET', 'http://example.com/user/login');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()
      ->addFilter(new TestFilter())
      ->match($context);
    $this->assertEquals('user.view', $match->getRouteName());
  }

  public function testEnhancer() {
    $request = new Request('GET', 'http://example.com/user/rhys');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()
      ->addEnhancer(new TestEnhancer())
      ->match($context);
    $this->assertEquals('user.view', $match->getRouteName());
    $this->assertEquals('rhys', $match->getRawParameter('user'));
    $this->assertEquals('rhys', $match->getParameter('user')->user);
  }

  public function testEnhancerSkip() {
    $request = new Request('GET', 'http://example.com/home/value');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()
      ->addEnhancer(new TestEnhancer())
      ->match($context);
    $this->assertEquals('home.action', $match->getRouteName());
    $this->assertEquals('value', $match->getParameter('action'));
    $this->assertFalse(array_key_exists('user', $match->getParameters()));
  }

  public function testDefaults() {
    $request = new Request('GET', 'http://example.com/home/');
    $context = $this->contextFactory->getRequestContext($request);

    $match = $this->getRouter()->match($context);
    $this->assertEquals('home.action', $match->getRouteName());
    $this->assertEquals('view', $match->getParameter('action'));
  }

}

class TestFilter implements FilterInterface {

  public function filter(CollectionInterface $collection, ContextInterface $context) {
    $collection->remove('user.login');
    return $collection;
  }

}

class TestEnhancer implements EnhancerInterface {

  public function applies(array $parameters, MatchInterface $match, ContextInterface $context) {
    return !empty($parameters['user']);
  }

  public function enhance(array $parameters, MatchInterface $match, ContextInterface $context) {
    $parameters['user'] = (object)['user' => $parameters['user']];
    return $parameters;
  }

}
