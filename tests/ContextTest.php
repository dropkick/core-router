<?php

namespace Dropkick\Core\Router;

use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase {

  public function testBaseUrl() {
    $base_url = 'https://example.com/base';
    $request = new Request('GET', 'https://example.com/base/example?test=1#fragment');
    $context = new Context($base_url, $request);

    $this->assertEquals('https', $context->getScheme());
    $this->assertEquals('443', $context->getPort());
    $this->assertEquals('example.com', $context->getHost());
    $this->assertEquals('example', $context->getPathInfo());
    $this->assertEquals($base_url, $context->getBaseUrl());
    $this->assertEquals('test=1', $context->getQueryString());
    $this->assertEquals('GET', $context->getMethod());
    $this->assertEquals($request, $context->getRequest());
  }

  public function testBuildUri() {
    $parts = parse_url('https://example.com:443/base?test=1#fragment');
    $url = Context::buildUrl($parts);
    $this->assertEquals('https://example.com/base?test=1#fragment', $url);

    $parts = parse_url('https://example.com:80/base?test=1#fragment');
    $url = Context::buildUrl($parts);
    $this->assertEquals('https://example.com:80/base?test=1#fragment', $url);

    $parts = parse_url('http://example.com:443/base?test=1#fragment');
    $url = Context::buildUrl($parts);
    $this->assertEquals('http://example.com:443/base?test=1#fragment', $url);

    $parts = parse_url('http://example.com:80/base?test=1#fragment');
    $url = Context::buildUrl($parts);
    $this->assertEquals('http://example.com/base?test=1#fragment', $url);
  }
}
