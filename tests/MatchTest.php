<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class MatchTest extends TestCase {
  
  public function testRawParameters() {
    $match = new Match('test', new Route(['path' => 'path']), ['head' => TRUE], ['tail' => TRUE]);
    $modified_match = $match->withRawParameters(['tail' => TRUE]);
    $this->assertEquals(['tail' => TRUE], $modified_match->getRawParameters());
    $this->assertNotEquals($match, $modified_match);
  }

}
