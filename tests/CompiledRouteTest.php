<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class CompiledRouteTest extends TestCase {

  public function testWithRegex() {
    $route = new CompiledRoute(['path' => 'test', 'regex' => 'none']);
    $modified_route = $route->withRegex('regex');
    $this->assertEquals('regex', $modified_route->getRegex());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithPrefix() {
    $route = new CompiledRoute(['path' => 'test', 'prefix' => 'none', 'regex' => 'none']);
    $modified_route = $route->withPrefix('prefix');
    $this->assertEquals('prefix', $modified_route->getPrefix());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithFit() {
    $route = new CompiledRoute(['path' => 'test', 'fit' => 10, 'regex' => 'none']);
    $modified_route = $route->withFit(15);
    $this->assertEquals(15, $modified_route->getFit());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithDepth() {
    $route = new CompiledRoute(['path' => 'test', 'depth' => 10, 'regex' => 'none']);
    $modified_route = $route->withDepth(15);
    $this->assertEquals(15, $modified_route->getDepth());
    $this->assertNotEquals($route, $modified_route);
  }

  public function testWithParameters() {
    $route = new CompiledRoute(['path' => 'test', 'parameters' => ['test'], 'regex' => 'none']);
    $modified_route = $route->withParameters(['fit']);
    $this->assertTrue($route->hasParameter('test'));
    $this->assertFalse($modified_route->hasParameter('test'));
    $this->assertTrue($modified_route->hasParameter('fit'));
    $this->assertNotEquals($route, $modified_route);
  }

  public function testDefinition() {
    $params = [
      'path' => 'test',
      'parameters' => ['parameters'],
      'regex' => 'regex',
      'depth' => 10,
      'prefix' => 'prefix',
    ];

    $route = new CompiledRoute($params);

    $definition = $route->getDefinition();
    foreach ($params as $key => $value) {
      $this->assertEquals($value, $definition[$key], 'testDefinition '. $key);
    }
  }

}
