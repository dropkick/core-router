<?php

namespace Dropkick\Core\Router\Filter;

use Dropkick\Core\Router\Collection;
use Dropkick\Core\Router\Compiler;
use Dropkick\Core\Router\Context;
use Dropkick\Core\Router\Exception\ResourceNotFoundException;
use Dropkick\Core\Router\Route;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;

class PrefixFilterTest extends TestCase {

  protected $routes;

  public function setUp(): void {
    $compiler = new Compiler();
    $routes = [
      'home' => new Route(['path' => '/home']),
      'home.action' => new Route(['path' => '/home/{action}', 'defaults' => ['action' => 'view'], 'methods' => ['GET']]),
      'user.view' => new Route(['path' => '/user/{user}']),
      'user.login' => new Route(['path' => '/user/login']),
      'methods' => new Route(['path' => '/methods', 'methods' => ['GET']])
    ];

    $this->routes = new Collection(array_map(function($r) use ($compiler) { return $compiler->compile($r); }, $routes));
  }

  public function testNoMatch() {
    $filter = new PrefixFilter();
    $request = new Request('GET', 'https://example.com/no/match');
    $context = new Context('https://example.com', $request);

    $this->expectException(ResourceNotFoundException::class);
    $filter->filter($this->routes, $context);
  }

  public function testMatchHome() {
    $filter = new PrefixFilter();
    $request = new Request('GET', 'https://example.com/home');
    $context = new Context('https://example.com', $request);

    $results = $filter->filter($this->routes, $context);
    $this->assertEquals(2, count($results));
  }

}
