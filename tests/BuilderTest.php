<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase {

  /**
   * @var \Dropkick\Core\Router\Factory
   */
  protected $factory;

  /**
   * @var \Dropkick\Core\Router\Compiler
   */
  protected $compiler;

  /**
   * @var \Dropkick\Core\Router\ProviderInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $provider;

  public function setUp(): void {
    $this->factory = new Factory();
    $this->compiler = new Compiler();
    $this->provider = $this->getMockBuilder(ProviderInterface::class)
      ->getMock();
  }

  protected function getBuilder() {
    return new Builder();
  }

  public function testRecursiveRebuild() {
    $builder = $this->getBuilder();
    $provider = $this->provider;
    $compiler = $this->compiler;
    $factory = $this->factory;

    $this->provider->method('getAllRoutes')->willReturnCallback(function() use ($builder, $factory, $provider, $compiler) {
      $builder->rebuild($factory, $compiler, $provider);
      return [];
    });

    $this->expectException(\RuntimeException::class);
    $this->expectExceptionMessage('Recursive router rebuild detected.');

    $builder->rebuild($factory, $compiler, $provider);
  }

  public function testCachedRoutes() {
    $builder = $this->getBuilder();
    $provider = $this->provider;
    $compiler = $this->compiler;
    $factory = $this->factory;
    $routes = [
      'test' => new Route(['path' => 'test'])
    ];

    $this->provider->method('getAllRoutes')->willReturn($routes, []);

    $content = $builder->rebuild($factory, $compiler, $provider);
    $this->assertEquals(1, count($content));
    $this->assertEquals(CompiledRoute::class, get_class($content->get('test')));

    $content = $builder->rebuild($factory, $compiler, $provider);
    $this->assertEquals(1, count($content));
    $this->assertEquals(CompiledRoute::class, get_class($content->get('test')));
  }

  public function testNeedsRebuild() {
    $builder = $this->getBuilder();
    $provider = $this->provider;
    $compiler = $this->compiler;
    $factory = $this->factory;
    $routes = [
      'test' => new Route(['path' => 'test'])
    ];
    $updated_route = [
      'home' => new Route(['path' => 'home'])
    ];

    $this->provider->method('getAllRoutes')->willReturn($routes, $updated_route);

    $content = $builder->rebuild($factory, $compiler, $provider);
    $this->assertEquals(1, count($content));
    $this->assertEquals(CompiledRoute::class, get_class($content->get('test')));

    $builder->setRebuildNeeded();

    $content = $builder->rebuild($factory, $compiler, $provider);
    $this->assertEquals(1, count($content));
    $this->assertEquals(CompiledRoute::class, get_class($content->get('home')));
  }

}
