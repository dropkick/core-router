<?php

namespace Dropkick\Core\Router;

use PHPUnit\Framework\TestCase;

class CompilerTest extends TestCase {

  protected function getCompiledRoute($path, $extras = []) {
    $route_compiler = new Compiler();
    $route = $route_compiler->compile(new Route(['path' => $path] + $extras));
    return $route;
  }

  /**
   * Tests Compiler::getFit().
   *
   * @param string $path
   *   A path whose fit will be calculated in the test.
   * @param int $expected
   *   The expected fit returned by Compiler::getFit()
   *
   * @dataProvider providerTestGetFit
   */
  public function testGetFit($path, $expected) {
    $route = $this->getCompiledRoute($path);
    $result = $route->getFit($path);
    $this->assertSame($expected, $result);
  }

  /**
   * Provides data for CompilerTest::testGetFit()
   *
   * @return array
   *   An array of arrays, where each inner array has the path whose fit is to
   *   be calculated as the first value and the expected fit as the second
   *   value.
   */
  public function providerTestGetFit() {
    return [
      ['test', 1],
      ['/testwithleadingslash', 1],
      ['testwithtrailingslash/', 1],
      ['/testwithslashes/', 1],
      ['test/with/multiple/parts', 15],
      ['test/with/{some}/slugs', 13],
      ['test/very/long/path/that/dropkick/7/could/not/have/handled', 2047],
    ];
  }

  /**
   * Confirms that a route compiles properly with the necessary data.
   */
  public function testCompilation() {
    $compiled = $this->getCompiledRoute('/test/{something}/more');

    $this->assertEquals($compiled->getFit(), 5, 'The fit was incorrect.');
  }

  /**
   * Confirms that a compiled route with default values has the correct outline.
   */
  public function testCompilationDefaultValue() {
    // Because "here" has a default value, it should not factor into the outline
    // or the fitness.
    $compiled = $this->getCompiledRoute('/test/{something}/more/{here}/{again}', [
      'defaults' => [
        'here' => 'there',
        'again' => 'everything',
      ]
    ]);

    $this->assertEquals($compiled->getFit(), 5, 'The fit was not correct.');
  }

  /**
   * @dataProvider provideCompileData
   */
  public function testCompile($name, $arguments, $prefix, $regex, $variables)
  {
    $compiler = new Compiler();
    $compiled = $compiler->compile(new Route($arguments));
    $this->assertEquals($prefix, $compiled->getPrefix(), $name.' (static prefix)');
    $this->assertEquals($regex, $compiled->getRegex(), $name.' (regex)');
    $this->assertEquals($variables, $compiled->getParameters(), $name.' (parameters)');
  }

  public function provideCompileData()
  {
    return [
      [
        'Static route',
        ['path' => '/foo'],
        'foo', '#^foo$#sDiu', [],
      ],

      [
        'Route with a variable',
        ['path' => '/foo/{bar}'],
        'foo', '#^foo/(?P<bar>.*?)$#sDiu', ['bar'],
      ],

      [
        'Route with a variable that has a default value',
        ['path' => '/foo/{bar}', 'defaults' => ['bar' => 'bar']],
        'foo', '#^foo(?:/(?P<bar>.*?))?$#sDiu', ['bar'],
      ],

      [
        'Route with several variables',
        ['path' => '/foo/{bar}/{foobar}'],
        'foo', '#^foo/(?P<bar>.*?)/(?P<foobar>.*?)$#sDiu', ['bar', 'foobar'],
      ],

      [
        'Route with several variables that have default values',
        ['path' => '/foo/{bar}/{foobar}', 'defaults' => ['bar' => 'bar', 'foobar' => '']],
        'foo', '#^foo(?:/(?P<bar>.*?)(?:/(?P<foobar>.*?))?)?$#sDiu', ['bar', 'foobar'],
      ],

      [
        'Route with several variables but some of them have no default values',
        ['path' => '/foo/{bar}/{foobar}', 'defaults' => ['bar' => 'bar']],
        'foo', '#^foo/(?P<bar>.*?)/(?P<foobar>.*?)$#sDiu', ['bar', 'foobar'],
      ],

      [
        'Route with an optional variable as the first segment',
        ['path' => '/{bar}', 'defaults' => ['bar' => 'bar']],
        '', '#^(?P<bar>.*?)?$#sDiu', ['bar'],
      ],

      [
        'Route with a requirement of 0',
        ['path' => '/{bar}', 'defaults' => ['bar' => null], 'requirements' => ['bar' => '0']],
        '', '#^(?P<bar>0)?$#sDiu', ['bar'],
      ],

      [
        'Route with an optional variable as the first segment with requirements',
        ['path' => '/{bar}', 'defaults' => ['bar' => 'bar'], 'requirements' => ['bar' => '(foo|bar)']],
        '', '#^(?P<bar>(foo|bar))?$#sDiu', ['bar'],
      ],

      [
        'Route with only optional variables',
        ['path' => '/{foo}/{bar}', 'defaults' => ['foo' => 'foo', 'bar' => 'bar']],
        '', '#^(?P<foo>.*?)?(?:/(?P<bar>.*?))?$#sDiu', ['foo', 'bar'],
      ],

      [
        'Static non UTF-8 route',
        ['path' => "/fo\xE9"],
        "fo\xE9", "#^fo\xE9$#sDiu", [],
      ],

      [
        'Route with an explicit UTF-8 requirement',
        ['path' => '/{bar}', 'defaults' => ['bar' => null], 'requirements' => ['bar' => '.'], 'options' => ['utf8' => true]],
        '', '#^(?P<bar>.)?$#sDiu', ['bar'],
      ],

    ];
  }

  public function testRouteWithSameVariableTwice() {
    $this->expectException(\LogicException::class);
    $this->getCompiledRoute('/{name}/{name}');
  }

  /**
   * @dataProvider getVariableNamesStartingWithADigit
   */
  public function testRouteWithVariableNameStartingWithADigit($name) {
    $this->expectException(\DomainException::class);
    $this->getCompiledRoute('/{' . $name . '}');
  }

  public function getVariableNamesStartingWithADigit() {
    return [
      ['09'],
      ['123'],
      ['1e2'],
    ];
  }

  public function testRouteWithTooLongVariableName() {
    $this->expectException(\DomainException::class);
    $this->getCompiledRoute(sprintf('/{%s}', str_repeat('a', Compiler::VARIABLE_MAXIMUM_LENGTH + 1)));
  }

  public function testRouteWithInvalidPattern() {
    $this->expectException(\DomainException::class);
    $this->getCompiledRoute('/{test}', ['requirements' => ['test' => '[']]);
  }

}
