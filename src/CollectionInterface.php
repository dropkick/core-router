<?php

namespace Dropkick\Core\Router;

/**
 * Interface CollectionInterface.
 *
 * A collection of routes, used to select the behaviour of the system
 * to implement.
 */
interface CollectionInterface extends \IteratorAggregate, \Countable {

  /**
   * Add a route to the collection.
   *
   * @param string $name
   *   The route name.
   * @param \Dropkick\Core\Router\RouteInterface $route
   *   The route object.
   *
   * @return static
   *   The collection object.
   */
  public function add($name, RouteInterface $route);

  /**
   * Get all the routes indexed by name.
   *
   * @return \Dropkick\Core\Router\RouteInterface[]
   *   The list of routes defined by the collection.
   */
  public function all();

  /**
   * Get the route matching the name.
   *
   * @param string $name
   *   The route name.
   *
   * @return \Dropkick\Core\Router\RouteInterface|null
   *   Returns the route, or NULL.
   */
  public function get($name);

  /**
   * Remove a route by the name.
   *
   * @param string $name
   *   The route name.
   *
   * @return static
   *   The collection object.
   */
  public function remove($name);

  /**
   * Add a collection to this collection.
   *
   * @param \Dropkick\Core\Router\CollectionInterface $collection
   *   Routes located in this collection, will be moved to the end
   *   of the collection during the addition.
   *
   * @return static
   *   The collection object.
   */
  public function addCollection(CollectionInterface $collection);

  /**
   * Set the collection of routes.
   *
   * @param \Dropkick\Core\Router\CollectionInterface $collection
   *   The collection object to copy.
   *
   * @return static
   *   The collection object.
   */
  public function setCollection(CollectionInterface $collection);

  /**
   * {@inheritdoc}
   */
  public function getIterator();

}
