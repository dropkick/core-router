<?php

namespace Dropkick\Core\Router;

/**
 * Class CompiledRoute.
 *
 * A generic implementation of the CompiledRouteInterface.
 */
class CompiledRoute extends Route implements CompiledRouteInterface {

  /**
   * The regex string.
   *
   * @var string
   */
  protected $regex;

  /**
   * The prefix string.
   *
   * @var string
   */
  protected $prefix;

  /**
   * The fitness level.
   *
   * @var int
   */
  protected $fit;

  /**
   * The depth.
   *
   * @var int
   */
  protected $depth;

  /**
   * The parameter list.
   *
   * @var string[]
   */
  protected $parameters;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $definition = []) {
    parent::__construct($definition);
    $this->regex = $definition['regex'];
    $this->prefix = !empty($definition['prefix']) && is_string($definition['prefix']) ?
      $definition['prefix'] : '';
    $this->fit = (int) (!empty($definition['fit']) && is_numeric($definition['fit']) ?
      $definition['fit'] : 0);
    $this->depth = (int) (!empty($definition['depth']) && is_numeric($definition['depth']) ?
      $definition['depth'] : 0);
    $this->parameters = !empty($definition['parameters']) && is_array($definition['parameters']) ?
      $definition['parameters'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRegex() {
    return $this->regex;
  }

  /**
   * {@inheritdoc}
   */
  public function withRegex($regex) {
    $route = clone $this;
    $route->regex = $regex;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefix() {
    return $this->prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function withPrefix($prefix) {
    $route = clone $this;
    $route->prefix = $prefix;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getFit() {
    return $this->fit;
  }

  /**
   * {@inheritdoc}
   */
  public function withFit($fit) {
    $route = clone $this;
    $route->fit = $fit;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getDepth() {
    return $this->depth;
  }

  /**
   * {@inheritdoc}
   */
  public function withDepth($depth) {
    $route = clone $this;
    $route->depth = $depth;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function hasParameter($parameter) {
    return in_array($parameter, $this->parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function withParameters(array $parameters) {
    $route = clone $this;
    $route->parameters = $parameters;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    $definition = parent::getDefinition();

    $definition += [
      'regex' => $this->regex,
      'prefix' => $this->prefix,
      'fit' => $this->fit,
      'depth' => $this->depth,
      'parameters' => $this->parameters,
    ];

    return $definition;
  }

}
