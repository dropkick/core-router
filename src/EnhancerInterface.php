<?php

namespace Dropkick\Core\Router;

/**
 * Interface EnhancerInterface.
 *
 * Enhancers convert parameters from their raw form, to a representational
 * form, which can be used internally in controllers and access checks to
 * provide proper context for the route.
 */
interface EnhancerInterface {

  /**
   * Confirm the enhancer applies to the route.
   *
   * @param array $parameters
   *   The raw parameters of the matched route.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return bool
   *   Confirmation that the enhancer applies to the matched route.
   */
  public function applies(array $parameters, MatchInterface $match, ContextInterface $context);

  /**
   * Update the parameters based on its own data and the request.
   *
   * @param array $parameters
   *   The additional parameters for the routing information.
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The matched route.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The request context.
   *
   * @return array
   *   The updated parameters. An enhancer MUST return the parameters so that
   *   the parameters are passed onto the next enhancer and updated matched
   *   route.
   */
  public function enhance(array $parameters, MatchInterface $match, ContextInterface $context);

}
