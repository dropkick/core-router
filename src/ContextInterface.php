<?php

namespace Dropkick\Core\Router;

/**
 * Interface ContextInterface.
 *
 * Provides the request context for the route.
 */
interface ContextInterface {

  /**
   * Return the base url for the request.
   *
   * @return string
   *   The base url of the context request.
   */
  public function getBaseUrl();

  /**
   * Return the path for the request.
   *
   * @return string
   *   The path string.
   */
  public function getPathInfo();

  /**
   * Return the HTTP method.
   *
   * @return string
   *   Always returns an upper-cased method name.
   */
  public function getMethod();

  /**
   * Return the host used to make the request.
   *
   * @return string
   *   The host name.
   */
  public function getHost();

  /**
   * Return the scheme used to make the request.
   *
   * @return string
   *   The scheme.
   */
  public function getScheme();

  /**
   * Return the port number of the request.
   *
   * @return string
   *   The port number as a string.
   */
  public function getPort();

  /**
   * Get the query string.
   *
   * @return string
   *   The additional query parameters used for the request.
   */
  public function getQueryString();

  /**
   * Get the request that generated the context.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   The originating HTTP request.
   */
  public function getRequest();

}
