<?php

namespace Dropkick\Core\Router;

use Psr\Http\Message\RequestInterface;

/**
 * Class ContextFactory.
 *
 * A generic implementation of the ContextFactoryInterface.
 */
class ContextFactory implements ContextFactoryInterface {

  /**
   * The base url the context factory uses when creating contexts.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * Get the base url for the context factory.
   *
   * @return string
   *   The base url.
   */
  public function getBaseUrl() {
    return $this->baseUrl;
  }

  /**
   * Set the base url for the context factory.
   *
   * @param string $base_url
   *   The base url.
   *
   * @return static
   *   The context factory object.
   */
  public function setBaseUrl($base_url) {
    $this->baseUrl = $base_url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestContext(RequestInterface $request) {
    $uri = $request->getUri();

    // Default base url to just the host name.
    if (!($base_url = $this->getBaseUrl())) {
      $base_url = Context::buildUrl([
        'scheme' => $uri->getScheme(),
        'host' => $uri->getHost(),
        'port' => $uri->getPort(),
      ]);
    }

    return new Context($base_url, $request);
  }

}
