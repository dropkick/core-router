<?php

namespace Dropkick\Core\Router;

/**
 * Interface RouterInterface.
 *
 * Provides the basic interface for matching a request to a route.
 */
interface RouterInterface {

  /**
   * Match the request to route.
   *
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context for the request.
   *
   * @return \Dropkick\Core\Router\MatchInterface
   *   The matched route.
   */
  public function match(ContextInterface $context);

}
