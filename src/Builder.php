<?php

namespace Dropkick\Core\Router;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Builder.
 *
 * A generic implementation of the BuilderInterface.
 */
class Builder implements BuilderInterface {

  /**
   * The flag indicating a build is already occurring.
   *
   * @var bool
   */
  protected $building;

  /**
   * The collection of routes that was originally built.
   *
   * @var \Dropkick\Core\Router\CollectionInterface
   */
  protected $collection;

  /**
   * {@inheritdoc}
   */
  public function rebuild(
    FactoryInterface $factory,
    CompilerInterface $compiler,
    ProviderInterface $provider,
    $force = FALSE
  ) {
    // Successful rebuild when not needed.
    if (!$this->isRebuildNeeded() && !$force) {
      return $this->getBuiltCollection();
    }

    if ($this->isBuilding()) {
      throw new \RuntimeException(FormattableString::create('Recursive router rebuild detected.'));
    }

    // Start the building process.
    $this->setBuilding(TRUE);

    // Create the collection of compiled routes.
    $collection = $factory->createCollection();
    foreach ($provider->getAllRoutes() as $name => $route) {
      $collection->add($name, $compiler->compile($route));
    }

    // Store the built collection.
    $this->collection = $collection;

    // Complete the building process.
    $this->setBuilding(FALSE);

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function setRebuildNeeded() {
    unset($this->collection);
    return $this;
  }

  /**
   * Confirm the rebuild is needed.
   *
   * @return bool
   *   Confirmation of the requirement for a rebuild.
   */
  protected function isRebuildNeeded() {
    return !isset($this->collection);
  }

  /**
   * Get the previously built collection.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   The collection that was built.
   */
  protected function getBuiltCollection() {
    return $this->collection;
  }

  /**
   * Set the flag to indicate the building process is taking place.
   *
   * @param bool $flag
   *   The building flag.
   *
   * @return static
   *   The builder object.
   */
  protected function setBuilding($flag) {
    $this->building = (bool) $flag;
    return $this;
  }

  /**
   * Confirm building is underway.
   *
   * @return bool
   *   The building flag.
   */
  protected function isBuilding() {
    return $this->building;
  }

}
