<?php

namespace Dropkick\Core\Router;

/**
 * Interface ProviderInterface.
 *
 * A ProviderInterface supplies a collection of routes that can be processed
 * via the FilterInterface, or optimized via the BuilderInterface.
 */
interface ProviderInterface {

  /**
   * Returns all the routes on the system.
   *
   * @return \Dropkick\Core\Router\RouteInterface[]
   *   An iterator of routes keyed by route name.
   */
  public function getAllRoutes();

  /**
   * Resets the route provider object.
   */
  public function reset();

}
