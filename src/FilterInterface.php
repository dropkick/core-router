<?php

namespace Dropkick\Core\Router;

/**
 * Interface FilterInterface.
 *
 * Filters allow the route to narrow the collection down based on whatever
 * criterion the system is implementing.
 */
interface FilterInterface {

  /**
   * Filter the collection against the context, returning all matching routes.
   *
   * @param \Dropkick\Core\Router\CollectionInterface $collection
   *   The collection against which to match. All routes are expected to
   *    be compiled routes, and will be ignored if they are not.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context that generated the request behaviour.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   A non-empty RouteCollection of matched routes.
   */
  public function filter(CollectionInterface $collection, ContextInterface $context);

}
