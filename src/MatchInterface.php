<?php

namespace Dropkick\Core\Router;

/**
 * Interface MatchInterface.
 *
 * The match interface defines the expected shape of a match. This represents
 * the combination of the route name, route object, and parameters that were
 * passed to the route.
 */
interface MatchInterface {

  /**
   * Returns the route name.
   *
   * @return string
   *   The route name.
   */
  public function getRouteName();

  /**
   * Returns the route object.
   *
   * @return \Dropkick\Core\Router\RouteInterface|null
   *   The route object.
   */
  public function getRouteObject();

  /**
   * Returns the processed value of a named route parameter.
   *
   * Raw URL parameters are processed by the parameter conversion system, which
   * does operations such as converting entity ID parameters to fully-loaded
   * entities. For example, the path node/12345 would have a raw node ID
   * parameter value of 12345, while the processed parameter value would be the
   * corresponding loaded node object.
   *
   * @param string $parameter_name
   *   The parameter name.
   *
   * @return mixed|null
   *   The parameter value. NULL if the route doesn't define the parameter or
   *   if the parameter value can't be determined from the request.
   */
  public function getParameter($parameter_name);

  /**
   * Returns the bag of all processed route parameters.
   *
   * Raw URL parameters are processed by the parameter conversion system, which
   * does operations such as converting entity ID parameters to fully-loaded
   * entities. For example, the path node/12345 would have a raw node ID
   * parameter value of 12345, while the processed parameter value would be the
   * corresponding loaded node object.
   *
   * @return array
   *   The parameter list.
   */
  public function getParameters();

  /**
   * Return an instance with these parameters.
   *
   * @param array $parameters
   *   The new parameters for the match.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withParameters(array $parameters);

  /**
   * Returns the raw value of a named route parameter.
   *
   * @param string $parameter_name
   *   The parameter name.
   *
   * @return string|null
   *   The raw (non-upcast) parameter value. NULL if the route doesn't define
   *   the parameter or if the raw parameter value can't be determined from the
   *   request.
   */
  public function getRawParameter($parameter_name);

  /**
   * Returns the bag of all raw route parameters.
   *
   * @return array
   *   The parameter list.
   */
  public function getRawParameters();

  /**
   * Return an instance with these parameters.
   *
   * @param array $params
   *   The new raw parameters for the match.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withRawParameters(array $params);

}
