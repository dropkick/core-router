<?php

namespace Dropkick\Core\Router;

/**
 * Interface BuilderInterface.
 *
 * This converts the routes into compiled routes so that they can be filtered
 * and selected by the RouterInterface.
 */
interface BuilderInterface {

  /**
   * Rebuilds the route information and dumps it.
   *
   * @param \Dropkick\Core\Router\FactoryInterface $factory
   *   The factory for creating new routes and collections.
   * @param \Dropkick\Core\Router\CompilerInterface $compiler
   *   The compiler used to convert routes into compiled versions.
   * @param \Dropkick\Core\Router\ProviderInterface $provider
   *   The provider used to supply the routes to be compiled.
   * @param bool $force
   *   The flag used to confirm forcing a rebuild regardless of
   *   the current state of the built routes.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   Returns the list of built routes.
   */
  public function rebuild(
    FactoryInterface $factory,
    CompilerInterface $compiler,
    ProviderInterface $provider,
    $force = FALSE
  );

  /**
   * Sets the router to be rebuilt next time rebuildIfNeeded() is called.
   *
   * @return static
   *   The builder object.
   */
  public function setRebuildNeeded();

}
