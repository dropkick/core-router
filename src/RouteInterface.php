<?php

namespace Dropkick\Core\Router;

/**
 * Interface RouteInterface.
 *
 * Provides an immutable route with basic information about the methods,
 * defaults, requirements and options that is uses.
 */
interface RouteInterface {

  /**
   * Get the path the route is mapping.
   *
   * @return string
   *   The route path without the preceding slash.
   */
  public function getPath();

  /**
   * Return an instance with the path.
   *
   * @param string $path
   *   The updated path for the route.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withPath($path);

  /**
   * Get the route matching methods.
   *
   * @return string[]
   *   The list of methods that the route utilizes.
   */
  public function getMethods();

  /**
   * Return an instance with the methods.
   *
   * @param array $methods
   *   The updated list of methods.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withMethods(array $methods);

  /**
   * Get the defaults of the route.
   *
   * @return array
   *   The defaults for parameters.
   */
  public function getDefaults();

  /**
   * Confirm the existence of the default.
   *
   * @param string $key
   *   The key for the parameter.
   *
   * @return bool
   *   Confirmation the parameter default exists.
   */
  public function hasDefault($key);

  /**
   * Get the default value for a key.
   *
   * @param string $key
   *   The key for the parameter.
   *
   * @return mixed
   *   The value of the parameter. Returns NULL if the parameter
   *   is not defined.
   */
  public function getDefault($key);

  /**
   * Return an instance with these defaults.
   *
   * @param array $defaults
   *   The new list of defaults for the route.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withDefaults(array $defaults);

  /**
   * Get the options of the route.
   *
   * @return array
   *   The options of the route.
   */
  public function getOptions();

  /**
   * Confirm there is an option.
   *
   * @param string $option
   *   The option key to verify.
   *
   * @return bool
   *   Confirmation of the existence of the key.
   */
  public function hasOption($option);

  /**
   * Get the option, with default for none.
   *
   * @param string $option
   *   The option key.
   * @param mixed $default
   *   A default value.
   *
   * @return mixed
   *   The value for the option. Returns default if the option is undefined.
   */
  public function getOption($option, $default = NULL);

  /**
   * Return an instance with these options.
   *
   * @param array $options
   *   The new list of options for the route.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withOptions(array $options);

  /**
   * Get the requirements for the route.
   *
   * @return array
   *   The requirements for the parameters.
   */
  public function getRequirements();

  /**
   * Return an instance with these requirements.
   *
   * @param array $requirements
   *   The new list of requirements for the route.
   *
   * @return static
   *   The newly created immutable object.
   */
  public function withRequirements(array $requirements);

  /**
   * Get the definition of the route.
   *
   * @return array
   *   The definition of route as an array.
   */
  public function getDefinition();

}
