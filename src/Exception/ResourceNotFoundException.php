<?php

namespace Dropkick\Core\Router\Exception;

/**
 * Class ResourceNotFoundException.
 *
 * Triggered when no routes are located that match the request.
 */
class ResourceNotFoundException extends \Exception {
}
