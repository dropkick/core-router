<?php

namespace Dropkick\Core\Router\Exception;

/**
 * Class MethodNotAllowedException.
 *
 * Triggered when there are no routes that match due to method restrictions.
 */
class MethodNotAllowedException extends \Exception {

  /**
   * The acceptable methods.
   *
   * @var string[]
   */
  protected $methods;

  /**
   * MethodNotAllowedException constructor.
   *
   * @param string[] $methods
   *   The methods that are acceptable.
   * @param string $message
   *   The exception message.
   * @param int $code
   *   The exception code.
   * @param \Throwable|null $previous
   *   The previous exception.
   */
  public function __construct(array $methods, $message = "", $code = 0, \Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
    $this->methods = array_map('strtoupper', $methods);
  }

  /**
   * Get the allowed methods.
   *
   * @return string[]
   *   The acceptable methods.
   */
  public function getMethods() {
    return $this->methods;
  }

}
