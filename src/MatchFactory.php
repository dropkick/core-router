<?php

namespace Dropkick\Core\Router;

/**
 * Class MatchFactory.
 *
 * A generic implementation of MatchFactoryInterface.
 */
class MatchFactory implements MatchFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createMatch($name, RouteInterface $route, array $raw_params, array $params) {
    return new Match($name, $route, $raw_params, $params);
  }

}
