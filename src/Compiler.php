<?php

namespace Dropkick\Core\Router;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Compiler.
 *
 * A generic implementation of the CompilerInterface.
 */
class Compiler implements CompilerInterface {

  /**
   * The maximum supported length of a PCRE subpattern name.
   *
   * @see http://pcre.org/current/doc/html/pcre2pattern.html#SEC16.
   *
   * @internal
   */
  const VARIABLE_MAXIMUM_LENGTH = 32;

  /**
   * {@inheritdoc}
   */
  public function compile(RouteInterface $route) {
    return new CompiledRoute($route->getDefinition() + $this->getDefinition($route));
  }

  /**
   * Generate the regex from the route.
   *
   * @param \Dropkick\Core\Router\RouteInterface $route
   *   The route object.
   *
   * @return array
   *   An array containing additional information about the route object.
   *   This is used to construct a compiled route.
   */
  protected function getDefinition(RouteInterface $route) {
    // Get the delimiter for the regexes.
    $delim = CompilerInterface::REGEX_DELIMITER;

    // Get the requirements.
    $requirements = $route->getRequirements();

    // Get the route path.
    $path = $route->getPath();

    // Split the path up on the slashes, ignoring multiple slashes in a row
    // or leading or trailing slashes.
    $parts = preg_split('@/+@', $path, NULL, PREG_SPLIT_NO_EMPTY);
    if ($parts === FALSE) {
      $parts = [$path];
    }

    // Save for loop calculation.
    $slashes = count($parts) - 1;

    // Process each part, into regex.
    $last_static = -1;
    $fit = 0;
    $prefix = [];
    $parameters = [];
    foreach ($parts as $key => $part) {
      // We only match whole path segments.
      if (!preg_match('/\{(\w+)\}/', $part, $match)) {
        // Calculate the appropriate fitness.
        $fit |= 1 << ($slashes - $key);

        // Keep a record of the prefix.
        if (!count($parameters)) {
          $prefix[] = $part;
        }

        // Ensure regex is quoted.
        $parts[$key] = preg_quote($part, $delim);

        // Keep record of the last static.
        $last_static = $key;
        continue;
      }

      // Get the parameter name.
      $param = $match[1];

      // Get the parameter pattern.
      $pattern = isset($requirements[$param]) ? $requirements[$param] : '.*?';

      // A PCRE subpattern name must start with a non-digit. Also a PHP
      // variable cannot start with a digit so the variable would not be
      // usable as an injectable argument.
      if (preg_match('/^\d/', $param)) {
        throw new \DomainException(
          FormattableString::create(
            'Variable name "{{ variable }}" cannot start with a digit in route pattern "{{ pattern }}". Please use a different name.',
            ['variable' => $param, 'pattern' => $pattern]
          )
        );
      }
      if (in_array($param, $parameters)) {
        throw new \LogicException(
          FormattableString::create(
            'Route pattern "{{ pattern }}" cannot reference variable name "{{ variable }}" more than once.',
            ['pattern' => $pattern, 'variable' => $param]
          )
        );
      }
      if (strlen($param) > self::VARIABLE_MAXIMUM_LENGTH) {
        throw new \DomainException(
          FormattableString::create(
            'Variable name "{{ variable }}" cannot be longer than {{ length }} characters in route pattern "{{ pattern }}". Please use a shorter name.',
            [
              'variable' => $param,
              'length' => self::VARIABLE_MAXIMUM_LENGTH,
              'pattern' => $pattern,
            ]
          )
        );
      }
      // Use the provided regex to search for items, and validate against
      // preg_match. We deliberately ignore any warnings or errors as they
      // should be caught by the exception.
      if (@preg_match($delim . $pattern . $delim, '') === FALSE) {
        throw new \DomainException(
          FormattableString::create(
            'Route "{{ path }}" parameter "{{ variable }}" pattern "{{ pattern }}" is invalid.',
            [
              'path' => $route->getPath(),
              'variable' => $param,
              'pattern' => $pattern,
            ]
          )
        );
      }

      // Keep track of the parameters.
      $parameters[$key] = $param;

      // Add as a capture.
      $parts[$key] = sprintf('(?P<%s>%s)', $param, $pattern);
    }

    // Get the defaults.
    $defaults = $route->getDefaults();

    // Convert every default option at the end to a capturing group.
    $optional = '';
    for ($i = $slashes; $i > $last_static; $i--) {
      // We stop converting into non-capturing groups.
      if (!array_key_exists($parameters[$i], $defaults)) {
        break;
      }

      // Add following to the optional stuff.
      if ($i > 0) {
        $optional = sprintf('(?:/%s%s)?', $parts[$i], $optional);
      }
      else {
        $optional = sprintf('%s?%s', $parts[$i], $optional);
      }
      unset($parts[$i]);

      // Fitness values are too far to the left at this point.
      $fit >>= 1;
    }

    return [
      'prefix' => implode('/', $prefix),
      'regex' => "{$delim}^" . implode('/', $parts) . "{$optional}\${$delim}" . 'sDiu',
      'depth' => count($parts),
      'parameters' => array_values($parameters),
      'fit' => $fit,
    ];
  }

}
