<?php

namespace Dropkick\Core\Router;

/**
 * Interface CompilerInterface.
 *
 * A compiler converts a route definition, into an actionable object that
 * can check itself against a path.
 */
interface CompilerInterface {

  /**
   * The regex delimiter used when checking expressions.
   */
  const REGEX_DELIMITER = '#';

  /**
   * Compile a route for use in fast pattern matching.
   *
   * @param \Dropkick\Core\Router\RouteInterface $route
   *   The route object.
   *
   * @return \Dropkick\Core\Router\CompiledRouteInterface
   *   The compiled route object.
   */
  public function compile(RouteInterface $route);

}
