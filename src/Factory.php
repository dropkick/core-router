<?php

namespace Dropkick\Core\Router;

/**
 * Class Factory.
 *
 * A generic implementation of FactoryInterface.
 */
class Factory implements FactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createRoute(array $definition) {
    return new Route($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection(array $routes = []) {
    return new Collection($routes);
  }

}
