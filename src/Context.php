<?php

namespace Dropkick\Core\Router;

use Dropkick\Core\Formattable\FormattableString;
use Psr\Http\Message\RequestInterface;

/**
 * Class Context.
 *
 * A generic implementation of the ContextInterface.
 */
class Context implements ContextInterface {

  /**
   * The base URL used for the request.
   *
   * @var string
   */
  protected $url;

  /**
   * The path used for the request.
   *
   * @var string
   */
  protected $path;

  /**
   * The port used for the request.
   *
   * @var string
   */
  protected $port;

  /**
   * The host used for the request.
   *
   * @var string
   */
  protected $host;

  /**
   * The scheme used for the request.
   *
   * @var string
   */
  protected $scheme;

  /**
   * The query string used for the request.
   *
   * @var string
   */
  protected $query;

  /**
   * The original request object.
   *
   * @var \Psr\Http\Message\RequestInterface
   */
  protected $request;

  /**
   * Context constructor.
   *
   * @param string $base_url
   *   The base URL used for the request.
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request object.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct($base_url, RequestInterface $request) {
    $base_parts = parse_url($base_url);
    if ($base_parts === FALSE) {
      throw new \InvalidArgumentException(FormattableString::create('Invalid base URL provided.'));
    }
    $base_parts += [
      'path' => '',
    ];

    $parts = parse_url((string) $request->getUri());
    if ($parts === FALSE) {
      throw new \InvalidArgumentException(FormattableString::create('Invalid request URL provided.'));
    }
    $parts += [
      'host' => '',
      'path' => '',
    ];

    $split = !empty($base_parts['path']) ? mb_strlen($base_parts['path']) : 0;

    $this->path = ltrim(mb_substr($parts['path'], $split), '/');
    $this->host = strtolower($parts['host']);
    $this->port = (string) (!empty($parts['port']) ? $parts['port'] : '443');
    $this->scheme = !empty($parts['scheme']) ? $parts['scheme'] : 'https';
    $this->query = !empty($parts['query']) ? $parts['query'] : '';

    // Creates the full proper base URL from the given details.
    $this->url = self::buildUrl([
      'scheme' => $this->scheme,
      'host' => $this->host,
      'path' => '/' . ltrim(mb_substr((string) $parts['path'], 0, $split), '/'),
      'port' => in_array($this->port, ['80', '443']) ? '' : $this->port,
    ]);

    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return $this->url;
  }

  /**
   * {@inheritdoc}
   */
  public function getPathInfo() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->request->getMethod();
  }

  /**
   * {@inheritdoc}
   */
  public function getHost() {
    return $this->host;
  }

  /**
   * {@inheritdoc}
   */
  public function getScheme() {
    return $this->scheme;
  }

  /**
   * {@inheritdoc}
   */
  public function getPort() {
    return $this->port;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryString() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequest() {
    return $this->request;
  }

  /**
   * Create a URL from result of parse_url.
   *
   * @param array $parts
   *   The parts of the url, as given by parse_url.
   *
   * @return string
   *   The URL constructed from the parts.
   */
  public static function buildUrl(array $parts) {
    // Get the scheme.
    $scheme = isset($parts['scheme']) ? ($parts['scheme'] . '://') : '';

    // Get the hostname.
    $host = $parts['host'] ?? '';

    // Only add the port if it is non-standard.
    $port = !empty($parts['port']) ? (':' . $parts['port']) : '';
    if ($port && $scheme) {
      switch (strtolower($scheme)) {
        case 'https://':
          $port = $port === ':443' ? '' : $port;
          break;

        case 'http://':
          $port = $port === ':80' ? '' : $port;
          break;
      }
    }

    $user = $parts['user'] ?? '';
    $pass = isset($parts['pass']) ? (':' . $parts['pass']) : '';
    $pass = ($user || $pass) ? ($pass . '@') : '';
    $path = $parts['path'] ?? '';
    $query = isset($parts['query']) ? ('?' . $parts['query']) : '';
    $fragment = isset($parts['fragment']) ? ('#' . $parts['fragment']) : '';

    return implode('', [
      $scheme,
      $user,
      $pass,
      $host,
      $port,
      $path,
      $query,
      $fragment,
    ]);
  }

}
