<?php

namespace Dropkick\Core\Router;

use Psr\Http\Message\RequestInterface;

/**
 * Interface ContextFactoryInterface.
 *
 * Provides a method for creating contexts from requests.
 */
interface ContextFactoryInterface {

  /**
   * Get a context based on the request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request object.
   *
   * @return \Dropkick\Core\Router\ContextInterface
   *   The context object.
   */
  public function getRequestContext(RequestInterface $request);

}
