<?php

namespace Dropkick\Core\Router;

/**
 * Interface MatchFactoryInterface.
 *
 * This is a factory object for overriding the creation of matches.
 */
interface MatchFactoryInterface {

  /**
   * Create a match object with the given details.
   *
   * @param string $name
   *   The route name.
   * @param \Dropkick\Core\Router\RouteInterface $route
   *   The route object.
   * @param array $raw_params
   *   The raw parameters.
   * @param array $params
   *   The processed parameters.
   *
   * @return \Dropkick\Core\Router\MatchInterface
   *   The match object.
   */
  public function createMatch($name, RouteInterface $route, array $raw_params, array $params);

}
