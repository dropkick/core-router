<?php

namespace Dropkick\Core\Router;

/**
 * Class Match.
 *
 * A generic implementation of MatchInterface.
 */
class Match implements MatchInterface {

  /**
   * The route name.
   *
   * @var string
   */
  protected $name;

  /**
   * The route object.
   *
   * @var \Dropkick\Core\Router\RouteInterface
   */
  protected $route;

  /**
   * The raw parameters.
   *
   * @var array
   */
  protected $rawParams;

  /**
   * The processed parameters.
   *
   * @var array
   */
  protected $params;

  /**
   * Match constructor.
   *
   * @param string $name
   *   The route name.
   * @param \Dropkick\Core\Router\RouteInterface $route
   *   The route object.
   * @param array $raw_params
   *   The raw parameters.
   * @param array $params
   *   The processed parameters.
   */
  public function __construct($name, RouteInterface $route, array $raw_params, array $params) {
    $this->name = $name;
    $this->route = $route;
    $this->rawParams = $raw_params;
    $this->params = $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteObject() {
    return $this->route;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter($parameter_name) {
    return array_key_exists($parameter_name, $this->params) ?
      $this->params[$parameter_name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function withParameters(array $parameters) {
    $match = clone $this;
    $match->params = $parameters;
    return $match;
  }

  /**
   * {@inheritdoc}
   */
  public function getRawParameter($parameter_name) {
    return array_key_exists($parameter_name, $this->rawParams) ?
      $this->rawParams[$parameter_name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRawParameters() {
    return $this->rawParams;
  }

  /**
   * {@inheritdoc}
   */
  public function withRawParameters(array $parameters) {
    $match = clone $this;
    $match->rawParams = $parameters;
    return $match;
  }

}
