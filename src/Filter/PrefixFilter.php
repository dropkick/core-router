<?php

namespace Dropkick\Core\Router\Filter;

use Dropkick\Core\Router\CollectionInterface;
use Dropkick\Core\Router\CompiledRouteInterface;
use Dropkick\Core\Router\ContextInterface;
use Dropkick\Core\Router\Exception\ResourceNotFoundException;
use Dropkick\Core\Router\FilterInterface;

/**
 * Class PrefixFilter.
 *
 * A filter that filters by matching the static prefix against the route.
 */
class PrefixFilter implements FilterInterface {

  /**
   * {@inheritdoc}
   */
  public function filter(CollectionInterface $collection, ContextInterface $context) {
    $path = $context->getPathInfo();

    foreach ($collection as $name => $route) {
      if ($route instanceof CompiledRouteInterface) {
        $prefix = strtolower($route->getPrefix());
        if (mb_substr($path, 0, strlen($prefix)) !== $prefix) {
          $collection->remove($name);
          continue;
        }

      }
    }

    if (!count($collection)) {
      throw new ResourceNotFoundException();
    }
    return $collection;
  }

}
