<?php

namespace Dropkick\Core\Router;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Router\Exception\MethodNotAllowedException;
use Dropkick\Core\Router\Exception\ResourceNotFoundException;

/**
 * Class Router.
 *
 * It consists of several steps, of which each are explained in more details
 * below:
 * 1. Get a collection of routes which potentially match the current request.
 *    This is done by the route builder, and the provider. See ::getRoutes().
 * 2. Filter the collection down further more. For example this filters out
 *    routes applying to other formats: See ::applyFilters()
 * 3. Find the best matching route out of the remaining ones, by applying a
 *    regex. See ::matchCollection().
 * 4. Enhance the list of route attributes, for example loading entity objects.
 *    See ::applyEnhancers().
 * */
class Router implements RouterInterface {

  /**
   * The route provider.
   *
   * @var \Dropkick\Core\Router\ProviderInterface
   */
  protected $provider;

  /**
   * The compiler that converts routes to actionable objects.
   *
   * @var \Dropkick\Core\Router\CompilerInterface
   */
  protected $compiler;

  /**
   * The factory that creates routes and collections.
   *
   * @var \Dropkick\Core\Router\FactoryInterface
   */
  protected $factory;

  /**
   * The route builder.
   *
   * @var \Dropkick\Core\Router\BuilderInterface
   */
  protected $builder;

  /**
   * The factory that creates a match from the route information.
   *
   * @var \Dropkick\Core\Router\MatchFactoryInterface
   */
  protected $matchFactory;

  /**
   * The context of the matched request.
   *
   * @var \Dropkick\Core\Router\ContextInterface
   */
  protected $context;

  /**
   * The filters applied to the route collection.
   *
   * @var \Dropkick\Core\Router\FilterInterface[]
   */
  protected $filters = [];

  /**
   * The enhancers used for the matched route.
   *
   * @var \Dropkick\Core\Router\EnhancerInterface[]
   */
  protected $enhancers = [];

  /**
   * Router constructor.
   *
   * @param \Dropkick\Core\Router\ProviderInterface $provider
   *   The provider.
   * @param \Dropkick\Core\Router\CompilerInterface $compiler
   *   The compiler.
   * @param \Dropkick\Core\Router\FactoryInterface $factory
   *   The factory.
   * @param \Dropkick\Core\Router\BuilderInterface $builder
   *   The builder.
   * @param \Dropkick\Core\Router\MatchFactoryInterface $matchFactory
   *   The match factory.
   */
  public function __construct(
    ProviderInterface $provider,
    CompilerInterface $compiler,
    FactoryInterface $factory,
    BuilderInterface $builder,
    MatchFactoryInterface $matchFactory
  ) {
    $this->provider = $provider;
    $this->compiler = $compiler;
    $this->factory = $factory;
    $this->builder = $builder;
    $this->matchFactory = $matchFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function match(ContextInterface $context) {
    // Get the routes.
    $collection = $this->getRoutes();

    // Apply the filters.
    $collection = $this->applyFilters($collection, $context);

    // Reapply fitness ordering to ensure proper ordering.
    $collection = $this->applyFitOrder($collection);

    // Get a match from the remaining collection.
    // Apply enhancers to it.
    $methods = [];
    if ($match = $this->matchCollection($context->getPathInfo(), $collection, $methods, $context)) {
      return $this->applyEnhancers($match, $context);
    }

    throw 0 < count($methods)
      ? new MethodNotAllowedException(array_unique($methods))
      : new ResourceNotFoundException(
        FormattableString::create(
          'No routes found for "{{ path }}".',
          ['path' => $context->getPathInfo()]
        )
      );
  }

  /**
   * Get the route collection.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   The list of routes available for the router.
   */
  protected function getRoutes() {
    return $this->builder->rebuild($this->factory, $this->compiler, $this->provider);
  }

  /**
   * Tries to match a URL with a set of routes.
   *
   * @param string $pathinfo
   *   The path info to be parsed.
   * @param \Dropkick\Core\Router\CollectionInterface $routes
   *   The set of routes.
   * @param array $methods
   *   The unsupported methods tested against for successful routes.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context object.
   *
   * @return \Dropkick\Core\Router\MatchInterface|null
   *   The matched route. NULL when there is no match.
   */
  protected function matchCollection($pathinfo, CollectionInterface $routes, array &$methods, ContextInterface $context) {
    // Ensure the pathinfo does not contain extraneous path separators.
    $pathinfo = ltrim($pathinfo, '/');

    // Cycle through each route to check for the first one that matches the
    // regex.
    foreach ($routes as $name => $route) {
      $compiledRoute = $this->compiler->compile($route);

      // Set the regex to use the compiled regex.
      $regex = $compiledRoute->getRegex();
      if (!preg_match($regex, ltrim($pathinfo, '/'), $matches)) {
        continue;
      }

      // Check HTTP method requirement.
      if ($requiredMethods = $compiledRoute->getMethods()) {
        // HEAD and GET are equivalent as per RFC.
        if ('HEAD' === $method = $context->getMethod()) {
          $method = 'GET';
        }

        if (!in_array($method, $requiredMethods)) {
          $methods = array_merge($methods, $requiredMethods);
          $routes->remove($name);
          continue;
        }
      }

      // Generate the parameters from the matches.
      $params = [];
      foreach ($compiledRoute->getParameters() as $key) {
        if (!empty($matches[$key])) {
          $params[$key] = $matches[$key];
        }
        elseif ($compiledRoute->hasDefault($key)) {
          $params[$key] = $compiledRoute->getDefault($key);
        }
      }

      return $this->matchFactory->createMatch($name, $route, $params, $params);
    }
    return NULL;
  }

  /**
   * Filter the collection using all the provided filters.
   *
   * @param \Dropkick\Core\Router\CollectionInterface $collection
   *   The collection of routes to filter.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context of the request.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   The filtered collection.
   *
   * @throws \Dropkick\Core\Router\Exception\ResourceNotFoundException
   *   The filters are expected to throw this when the collection is empty.
   */
  protected function applyFilters(CollectionInterface $collection, ContextInterface $context) {
    // Route filters are expected to throw an exception themselves if they
    // end up filtering the list down to 0.
    foreach ($this->filters as $filter) {
      $collection = $filter->filter($collection, $context);
    }

    return $collection;
  }

  /**
   * Apply enhancement of the route by processing the parameters.
   *
   * @param \Dropkick\Core\Router\MatchInterface $match
   *   The match for the request.
   * @param \Dropkick\Core\Router\ContextInterface $context
   *   The context of the request.
   *
   * @return \Dropkick\Core\Router\MatchInterface
   *   The enhanced match.
   */
  protected function applyEnhancers(MatchInterface $match, ContextInterface $context) {
    $parameters = $match->getRawParameters();
    foreach ($this->enhancers as $enhancer) {
      if ($enhancer instanceof EnhancerInterface && !$enhancer->applies($parameters, $match, $context)) {
        continue;
      }
      $parameters = $enhancer->enhance($parameters, $match, $context);
    }

    return $match->withParameters($parameters);
  }

  /**
   * Reapplies the fit order to a RouteCollection object.
   *
   * Route filters can reorder route collections. For example, routes with an
   * explicit _format requirement will be preferred. This can result in a less
   * fit route being used. For example, as a result of filtering /user/% comes
   * before /user/login. In order to not break this fundamental property of
   * routes, we need to reapply the fit order. We also need to ensure that order
   * within each group of the same fit is preserved.
   *
   * @param \Dropkick\Core\Router\CollectionInterface $collection
   *   The route collection.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   The reordered route collection.
   */
  protected function applyFitOrder(CollectionInterface $collection) {
    $buckets = [];
    // Sort all the routes by fit descending.
    foreach ($collection->all() as $name => $route) {
      $compiledRoute = $this->compiler->compile($route);
      $fit = $compiledRoute->getFit();
      $buckets += [$fit => []];
      $buckets[$fit][] = [$name, $compiledRoute];
    }
    krsort($buckets);

    $flattened = array_reduce($buckets, 'array_merge', []);

    // Add them back onto a new route collection.
    $collection = $this->factory->createCollection();
    foreach ($flattened as $pair) {
      $name = $pair[0];
      $route = $pair[1];
      $collection->add($name, $route);
    }
    return $collection;
  }

  /**
   * Add a filter to reduce the matches to cycle through.
   *
   * @param \Dropkick\Core\Router\FilterInterface $filter
   *   The filter to add.
   *
   * @return static
   *   The original object.
   */
  public function addFilter(FilterInterface $filter) {
    $this->filters[] = $filter;
    return $this;
  }

  /**
   * Add an enhancer to convert routing information, parameters, etc.
   *
   * @param \Dropkick\Core\Router\EnhancerInterface $enhancer
   *   The enhancer to add.
   *
   * @return static
   *   The original object.
   */
  public function addEnhancer(EnhancerInterface $enhancer) {
    $this->enhancers[] = $enhancer;
    return $this;
  }

}
