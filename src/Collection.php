<?php

namespace Dropkick\Core\Router;

/**
 * Class Collection.
 *
 * A generic implementation of CollectionInterface.
 */
class Collection implements CollectionInterface {

  /**
   * The route objects.
   *
   * @var \Dropkick\Core\Router\RouteInterface[]
   */
  protected $collection = [];

  /**
   * Collection constructor.
   *
   * @param array $collection
   *   The routes to initialize the collection.
   */
  public function __construct(array $collection = []) {
    foreach ($collection as $name => $item) {
      $this->add($name, $item);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function add($name, RouteInterface $route) {
    $this->collection[$name] = $route;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function all() {
    return $this->collection;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return isset($this->collection[$name]) ? $this->collection[$name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function remove($name) {
    unset($this->collection[$name]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCollection(CollectionInterface $collection) {
    foreach ($collection as $name => $route) {
      // Move any existing named route to the end.
      unset($this->collection[$name]);
      $this->collection[$name] = $route;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCollection(CollectionInterface $collection) {
    $this->collection = [];
    return $this->addCollection($collection);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->collection);
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator() {
    return new \ArrayIterator($this->collection);
  }

}
