<?php

namespace Dropkick\Core\Router;

/**
 * Interface CompiledRouteInterface.
 *
 * A compiled route provides a means to convert a path into a route, including
 * parameters that the route is expecting.
 */
interface CompiledRouteInterface extends RouteInterface {

  /**
   * Get the regular expression used to match the URL.
   *
   * @return string
   *   The regex string.
   */
  public function getRegex();

  /**
   * Return an instance using regex.
   *
   * @param string $regex
   *   The regex string.
   *
   * @return static
   *   A newly created immutable object.
   */
  public function withRegex($regex);

  /**
   * Get the static prefix of the path.
   *
   * This is the starting part of the path, that is not subject to parameters.
   *
   * @return string
   *   The prefix string.
   */
  public function getPrefix();

  /**
   * Return an instance with the prefix.
   *
   * @param string $prefix
   *   The prefix string.
   *
   * @return static
   *   A newly created immutable object.
   */
  public function withPrefix($prefix);

  /**
   * Get the importance of the path, relative to others.
   *
   * This emphasises static url segments over parameterized ones.
   *
   * @return int
   *   The fitness level.
   */
  public function getFit();

  /**
   * Return an instance with this fitness.
   *
   * @param int $fit
   *   The fitness level.
   *
   * @return static
   *   A newly created immutable object.
   */
  public function withFit($fit);

  /**
   * The number of segments in the path.
   *
   * @return int
   *   The depth.
   */
  public function getDepth();

  /**
   * Return an instance with this depth.
   *
   * @param int $depth
   *   The depth.
   *
   * @return static
   *   A newly created immutable object.
   */
  public function withDepth($depth);

  /**
   * Get the parameters used by the route.
   *
   * @return string[]
   *   The parameters as values in an array.
   */
  public function getParameters();

  /**
   * Confirm the existence of parameter.
   *
   * @param string $parameter
   *   The parameter.
   *
   * @return bool
   *   Confirms the existence of the parameter.
   */
  public function hasParameter($parameter);

  /**
   * Return an instance with these parameters.
   *
   * @param string[] $parameters
   *   The new parameters for the compiled route.
   *
   * @return static
   *   A newly created immutable object.
   */
  public function withParameters(array $parameters);

}
