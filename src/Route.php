<?php

namespace Dropkick\Core\Router;

/**
 * Class Route.
 *
 * A generic implementation of the RouteInterface.
 */
class Route implements RouteInterface {

  /**
   * The path of the route.
   *
   * @var string
   */
  protected $path;

  /**
   * The method restriction of the route.
   *
   * @var string[]
   */
  protected $methods;

  /**
   * The parameter defaults of the route.
   *
   * @var array
   */
  protected $defaults;

  /**
   * The options for the route.
   *
   * @var array
   */
  protected $options;

  /**
   * The parameter requirements for the route.
   *
   * @var array
   */
  protected $requirements;

  /**
   * Route constructor.
   *
   * @param array $definition
   *   The definition of the route.
   */
  public function __construct(array $definition = []) {
    $this->path = $definition['path'];
    $this->methods = !empty($definition['methods']) && is_array($definition['methods']) ?
      $definition['methods'] : [];
    $this->defaults = !empty($definition['defaults']) && is_array($definition['defaults']) ?
      $definition['defaults'] : [];
    $this->options = !empty($definition['options']) && is_array($definition['options']) ?
      $definition['options'] : [];
    $this->requirements = !empty($definition['requirements']) && is_array($definition['requirements']) ?
      $definition['requirements'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function withPath($path) {
    $route = clone $this;
    $route->path = $path;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethods() {
    return $this->methods;
  }

  /**
   * {@inheritdoc}
   */
  public function withMethods(array $methods) {
    $route = clone $this;
    $route->methods = $methods;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaults() {
    return $this->defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefault($key) {
    return array_key_exists($key, $this->defaults);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefault($key) {
    return array_key_exists($key, $this->defaults) ? $this->defaults[$key] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function withDefaults(array $defaults) {
    $route = clone $this;
    $route->defaults = $defaults;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($option, $default = NULL) {
    return array_key_exists($option, $this->options) ?
      $this->options[$option] :
      $default;
  }

  /**
   * {@inheritdoc}
   */
  public function hasOption($option) {
    return array_key_exists($option, $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function withOptions(array $options) {
    $route = clone $this;
    $route->options = $options;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequirements() {
    return $this->requirements;
  }

  /**
   * {@inheritdoc}
   */
  public function withRequirements(array $requirements) {
    $route = clone $this;
    $route->requirements = $requirements;
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    $definition = [
      'path' => $this->path,
    ];
    if (!empty($this->methods)) {
      $definition['methods'] = $this->methods;
    }
    if (!empty($this->defaults)) {
      $definition['defaults'] = $this->defaults;
    }
    if (!empty($this->options)) {
      $definition['options'] = $this->options;
    }
    if (!empty($this->requirements)) {
      $definition['requirements'] = $this->requirements;
    }
    return $definition;
  }

}
