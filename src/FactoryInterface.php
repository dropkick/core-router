<?php

namespace Dropkick\Core\Router;

/**
 * Interface FactoryInterface.
 *
 * Provides a factory that allows overriding of the creation of routes and
 * collections.
 */
interface FactoryInterface {

  /**
   * Create a route.
   *
   * @param array $definition
   *   The route definition.
   *
   * @return \Dropkick\Core\Router\RouteInterface
   *   The route object.
   */
  public function createRoute(array $definition);

  /**
   * Create a collection.
   *
   * @param \Dropkick\Core\Router\RouteInterface[] $routes
   *   The routes to be added to the collection.
   *
   * @return \Dropkick\Core\Router\CollectionInterface
   *   The route collection.
   */
  public function createCollection(array $routes = []);

}
